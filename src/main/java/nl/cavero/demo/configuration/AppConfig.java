package nl.cavero.demo.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("nl.cavero.demo.configuration")
public class AppConfig {
}
