package nl.cavero.demo.endpoint;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/helloworld")
public class HelloWorldEndpoint {

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity sayHello() {
        return ResponseEntity.ok("hello world!");
    }
}
